# animation_events.js

A tiny script to manage an global animation and animation events.

## Start the global animation process

To start an animation use `start_animation` function. It need a frames per
second integer value as argument :

`start_animation(60); // Will start an 60fps animation`

It rely on `requestAnimationFrame` as decribed in this [Stackoverflow
post](https://stackoverflow.com/a/19772220)

## Pause the global animation

To pause an animation use `pause_animation`

`pause_animation();`

This will pause all animation events.

## Resume the global animation

To resume an animation use `resume_animation`

`resume_animation();`

This will resume all animation events.

## Start an animation event

To animate something specifically use `start_animation_event`. It need three
arguments, an integer value and a function that itself need an arbitrary
argument.

Here the method :

`let animation_event = start_animation_event(frame_divider, function(local_frame_index){animation_event_code});`

### About `frame_divider`

The animation event speed adjustement, for a given value of 2 in a 60 fps context the animation event will be at 30 fps

### About `local_frame_index`

Argument for catching the local frame index number (the speed adjustement obtained by `frame_divider`) given by the global animation draw (see draw function)

### About `animation_event_code`

Your animation event code

## Pause animation event

To pause an animation event use `pause_animation_event` with a animation event stored in a variable as argument.

`pause_animation_event(animation_event)`

This will pause only the animation event specified and let the rest of the animation going on.

## Resume animation event

To resume an animation event use `resume_animation_event` with a animation event stored in a variable as argument.

`resume_animation_event(animation_event)`

This will resume only the animation event specified and let the rest of the animation going on or at pause.

## Change animation event speed

To change animation event speed use `change_animation_event_speed` with an animation event stored in a variable and a `frame_divider` value (see _About `frame_divider`_ part) as arguments.

`change_animation_event_speed(animation_event, frame_divider);`

This will replace the `frame_divider` value of the animation event.

## License notice

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>. 
